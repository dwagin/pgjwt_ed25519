EXTENSION = pgjwt_ed25519
DATA = pgjwt_ed25519--0.1.sql

# postgres build stuff
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
