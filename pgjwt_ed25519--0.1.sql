\echo 'Use "CREATE EXTENSION pgjwt_ed25519" to load this file.' \quit

CREATE OR REPLACE FUNCTION url_encode(data bytea)
    RETURNS text
    LANGUAGE sql
    IMMUTABLE PARALLEL SAFE
AS
$$
SELECT translate(encode(data, 'base64'), E'+/=\n', '-_');
$$;

CREATE OR REPLACE FUNCTION url_decode(data text)
    RETURNS bytea
    LANGUAGE sql
    IMMUTABLE PARALLEL SAFE
AS
$$
WITH
    t AS (SELECT translate(data, '-_', '+/') AS trans),
    rem AS (SELECT length(t.trans) % 4 remainder FROM t) -- compute padding size
SELECT decode(t.trans || CASE WHEN rem.remainder > 0 THEN repeat('=', (4 - rem.remainder)) ELSE '' END, 'base64')
FROM t,
     rem;
$$;

CREATE OR REPLACE FUNCTION sign(payload json, public_key bytea, private_key bytea)
    RETURNS text
    LANGUAGE sql
    IMMUTABLE PARALLEL SAFE
AS
$$
WITH
    signables AS (
        SELECT @extschema@.url_encode(convert_to('{"alg":"EdDSA","typ":"JWT"}', 'utf8')) || '.' ||
               @extschema@.url_encode(convert_to(payload::text, 'utf8')) AS data)
SELECT signables.data || '.' || @extschema@.url_encode(ed25519.sign(signables.data, public_key, private_key))
FROM signables;
$$;

CREATE OR REPLACE FUNCTION verify(token text, public_key bytea)
    RETURNS table
            (
                header  json,
                payload json,
                valid   boolean
            )
    LANGUAGE sql
    IMMUTABLE PARALLEL SAFE
AS
$$
SELECT convert_from(@extschema@.url_decode(r[1]), 'utf8')::json AS header,
       convert_from(@extschema@.url_decode(r[2]), 'utf8')::json AS payload,
       ed25519.verify(r[1] || '.' || r[2], @extschema@.url_decode(r[3]), public_key) AS valid
FROM regexp_split_to_array(token, '\.') r;
$$;
